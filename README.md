
## 简介
Xyy Admin，开箱即用的前后端分离后台权限系统，集成Sa-Token权限认证框架，提供一套企业级前后端分离框架，基于该系统可以快速实现企业级二次开发，提供登录认证，权限验证，访问次数限制等安全措施。以及用户管理、角色管理、权限管理、部门管理、岗位管理、数据字典、
业务日志监控等系统管理基础功能。


- Xyy Admin  前端                [xyy-admin-ui](https://gitee.com/xyy12611/xyy-admin-ui) 
- Xyy Admin  后端                [springboot-xyy-admin-v3](https://gitee.com/xyy12611/springboot-xyy-admin-v3) 
- Sa-Token  权限认证框架          [Sa-Token](https://gitee.com/dromara/sa-token)


## 前序准备

你需要在本地安装 [node](http://nodejs.org/) 和 [git](https://git-scm.com/)。本项目技术栈基于 TypeScript、[ES2015+](http://es6.ruanyifeng.com/)、[vue3](https://v3.cn.vuejs.org/)、[vuex](https://vuex.vuejs.org/zh-cn/)、[vue-router](https://router.vuejs.org/zh-cn/) 、 [Ant-Design-Vue](https://www.antdv.com/docs/vue/introduce-cn/)，所有的请求数据都使用[springboot-xyy-admin-v3](https://gitee.com/xyy12611/springboot-xyy-admin-v3) 服务获取，提前了解和学习这些知识会对使用本项目有很大的帮助。

<p align="center">
  <img width="900" src="https://gitee.com/xyy12611/xyy-admin-ui/raw/master/src/assets/images/link-admin-v3.png">
  <img width="900" src="https://gitee.com/xyy12611/xyy-admin-ui/raw/master/src/assets/images/link-admin-v3-1.png">
  <img width="900" src="https://gitee.com/xyy12611/xyy-admin-ui/raw/master/src/assets/images/link-admin-v3-2.png">
</p>

## 开发


```bash
# 克隆项目
git https://gitee.com/xyy12611/xyy-admin-ui.git

# 进入项目目录
cd xyy-admin-ui

# 安装依赖
npm install

# 可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm start
```

浏览器访问 http://localhost:3100/

登录账号  admin/123456

## 发布


# 构建生产环境
npm run build

## 加入讨论组

 <p align="left">

</p>

## 致谢：

本项目前端基于vue-vben-admin，感谢vue-vben-admin 的作者，致敬！
