import { UploadApiResult } from './model/uploadModel';
import { defHttp } from '/@/utils/http/axios';
import { UploadFileParams } from '/#/axios';
import { useGlobSetting } from '/@/hooks/setting';
const globSetting = useGlobSetting();
const apiUrl = globSetting.apiUrl;
enum Api {
  uploadUrl = '/rest/sysfile/updateAvatar',
  uploadUserAvatar = '/rest/sysfile/uploadUserAvatar',
  uploadTinymce = '/rest/sysfile/uploadTinymce',
}

/**
 * @description: Upload interface
 */
export function uploadApi(
  params: UploadFileParams,
  onUploadProgress: (progressEvent: ProgressEvent) => void,
) {
  return defHttp.uploadFile<UploadApiResult>(
    {
      url: Api.uploadUrl,
      onUploadProgress,
    },
    params,
  );
}

export function uploadUserAvatar(
  params: UploadFileParams,
  onUploadProgress: (progressEvent: ProgressEvent) => void,
) {
  return defHttp.uploadFile<UploadApiResult>(
    {
      url: Api.uploadUserAvatar,
      onUploadProgress,
    },
    params,
  );
}

export function uploadTinymce(
  params: UploadFileParams,
  onUploadProgress: (progressEvent: ProgressEvent) => void,
) {
  return defHttp.uploadFile<UploadApiResult>(
    {
      url: Api.uploadTinymce,
      onUploadProgress,
    },
    params,
  );
}

export const userAvatarApi = apiUrl + '/rest/sysfile/getUserAvatar/';

export const downLoadTinymceApi = apiUrl + '/rest/sysfile/downLoadTinymce/';
