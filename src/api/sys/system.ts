import { defHttp } from '/@/utils/http/axios';

enum Api {
  getUserListPage = '/rest/user/getUserListPage',
  addAccount = '/rest/user/add',
  updateAccount = '/rest/user/update',
  deleteAccount = '/rest/user/delete',

  getDeptList = '/rest/department/getDeptList',
  addDept = '/rest/department/add',
  UpdateDept = '/rest/department/update',
  deleteDept = '/rest/department/delete',

  getPermissionList = '/rest/permission/getPermissionList',
  getPermissionListByRole = '/rest/permission/getPermissionListByRole',
  addPermission = '/rest/permission/add',
  updatePermission = '/rest/permission/update',
  deletePermission = '/rest/permission/delete',

  getDataScopeListByRole = '/rest/role/getDataScopeListByRole',
  getRoleListPage = '/rest/role/getListPage',
  getRoleList = '/rest/role/getRoleList',
  addRole = '/rest/role/add',
  updateRole = '/rest/role/update',
  deleteRole = '/rest/role/delete',
  addDataScope = '/rest/role/addDataScope',

  getDictAllList = '/rest/dict/getDictAllList',
  addDict = '/rest/dict/addDict',
  updateDict = '/rest/dict/updateDict',
  deleteDict = '/rest/dict/deleteDict',

  getDictItemListPage = '/rest/dict/getDictItemListPage',
  addDictItem = '/rest/dict/addDictItem',
  updateDictItem = '/rest/dict/updateDictItem',
  deleteDictItem = '/rest/dict/deleteDictItem',
}

export const getDictItemListPage = (params) =>
  defHttp.get({ url: Api.getDictItemListPage, params });
export const addDictItem = (params) => defHttp.post({ url: Api.addDictItem, params });
export const updateDictItem = (params) => defHttp.post({ url: Api.updateDictItem, params });
export const deleteDictItem = (params) => defHttp.get({ url: Api.deleteDictItem, params });

export const getDictAllList = (params) => defHttp.get({ url: Api.getDictAllList, params });
export const addDict = (params) => defHttp.post({ url: Api.addDict, params });
export const updateDict = (params) => defHttp.post({ url: Api.updateDict, params });
export const deleteDict = (params) => defHttp.get({ url: Api.deleteDict, params });

export const getAccountListPage = (params) => defHttp.get({ url: Api.getUserListPage, params });
export const addAccount = (params) => defHttp.post({ url: Api.addAccount, params });
export const updateAccount = (params) => defHttp.post({ url: Api.updateAccount, params });
export const deleteAccount = (params) => defHttp.get({ url: Api.deleteAccount, params });

export const getDeptList = (params) => defHttp.get({ url: Api.getDeptList, params });
export const addDept = (params) => defHttp.post({ url: Api.addDept, params });
export const updateDept = (params) => defHttp.post({ url: Api.UpdateDept, params });
export const deleteDept = (params) => defHttp.get({ url: Api.deleteDept, params });

export const getPermissionList = (params) => defHttp.get({ url: Api.getPermissionList, params });
export const addPermission = (params) => defHttp.post({ url: Api.addPermission, params });
export const updatePermission = (params) => defHttp.post({ url: Api.updatePermission, params });
export const deletePermission = (params) => defHttp.get({ url: Api.deletePermission, params });
export const getPermissionListByRole = (params) =>
  defHttp.get({ url: Api.getPermissionListByRole, params });

export const getDataScopeListByRole = (params) =>
  defHttp.get({ url: Api.getDataScopeListByRole, params });
export const addDataScope = (params) => defHttp.post({ url: Api.addDataScope, params });

export const getRoleListPage = (params) => defHttp.get({ url: Api.getRoleListPage, params });
export const getRoleList = (params) => defHttp.get({ url: Api.getRoleList, params });
export const addRole = (params) => defHttp.post({ url: Api.addRole, params });
export const updateRole = (params) => defHttp.post({ url: Api.updateRole, params });
export const deleteRole = (params) => defHttp.get({ url: Api.deleteRole, params });
