import { defHttp } from '/@/utils/http/axios';

enum Api {
  getBLogListPage = '/rest/monitor/blog/getListPage',
  getOnlineUserListPage = '/rest/monitor/onlineUser/getListPage',
  Kickout = '/rest/monitor/onlineUser/kickout',
}

export const getBLogListPage = (params) => defHttp.get({ url: Api.getBLogListPage, params });
export const getOnlineUserListPage = (params) =>
  defHttp.get({ url: Api.getOnlineUserListPage, params });
export const Kickout = (params) => defHttp.get({ url: Api.Kickout, params });
