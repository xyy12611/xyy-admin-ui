import Icon from './src/Icon.vue';
import SvgIcon from './src/SvgIcon.vue';
import IconPicker from './src/IconPicker.vue';
/**
 * 图标
 */
export { Icon, IconPicker, SvgIcon };

export default Icon;
