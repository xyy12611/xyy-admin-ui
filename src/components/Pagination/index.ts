import { withInstall } from '/@/utils';

import pagination from './Index.vue';
/**
 * 分页组件
 */
export const Pagination = withInstall(pagination);
