import { withInstall } from '/@/utils';
import dropdown from './src/Dropdown.vue';
/**
 * 下拉菜单
 */
export * from './src/typing';
export const Dropdown = withInstall(dropdown);
